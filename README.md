# Final Project 

**Group Members:**

*  Diayana Andabo

*  Aaleyah Gibson

**Synopsis:**

*  This game is ran through a rasbperry pi, it is a 4x4 tic-tac-toe game that uses Beyonce and Dr.Gardner's instead of just X's and O's. 
   This game allows for one player to play the computer as an opponent, to either win, lose or draw. 

**Instructions:**
* First you choose whether you would like to be X or O as a gamer tag by using the keyboard emulator buttons. 
 The Yellow button is X and the Green Button is O.

* Then you click the box under the photo of Dr.Gardner or Beyonce to choose which character you would like to be to begin the game.

* After the game begins click on the square you would like your character to choose, the objective of the game is to get 
 four in a row accross of your character or four in a row diagonal of your character.

* When the game ends it will let you know if you won, lost or if it was a draw 

* After the results the computer allows you to choose whether you want to play another game by clicking the blue button attatched to the 
 keyboard emulator.

* If you choose to play again, the game begins all over again and you begin it by choosing your character.



